package com.example.config;

import android.content.Context;
import android.view.View;

/**
 * Created by Филипп on 25.08.2017.
 */

public abstract class Child {

    protected String name;
    protected Context mContext;

    protected int viewCode;

    public String getName() {
        return name;
    }

    public int getViewCode() {
        return viewCode;
    }

    public Child(String name, Context context) {
        this.name = name;
        this.mContext = context;
    }

    public abstract void setChild(View convertView);
}
