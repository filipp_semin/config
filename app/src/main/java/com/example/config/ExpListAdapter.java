package com.example.config;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by Филипп on 23.08.2017.
 */

public class ExpListAdapter extends BaseExpandableListAdapter {

    private ArrayList<ArrayList<Child>> mGroups;
    private ArrayList<String> groupNames;
    private Context mContext;

    public ExpListAdapter(Context context, ArrayList<ArrayList<Child>> groups, ArrayList<String> groupNames) {
        mContext = context;
        mGroups = groups;
        this.groupNames = groupNames;
    }

    public int getGroupCount(){
        return mGroups.size();
    }

    public int getChildrenCount(int groupPosition){
        return mGroups.get(groupPosition).size();
    }

    public Object getGroup(int groupPosition){
        return mGroups.get(groupPosition);
    }

    public Object getChild(int groupPosition, int childPosition){
        return mGroups.get(groupPosition).get(childPosition);
    }

    public long getGroupId(int groupPosition){
        return(groupPosition);
    }

    public long getChildId(int groupPosition, int childPosition){
        return(childPosition);
    }

    public boolean hasStableIds(){
        return true;
    }

    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent){
        if(convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.group_view, null);
        }

        /*if(isExpanded){
            ;
        } else {
            ;
        }*/

        TextView textGroup = (TextView) convertView.findViewById(R.id.textGroup);
        textGroup.setText(groupNames.get(groupPosition));

        return convertView;
    }

    public View getChildView(int groupPosition, int childPosition, boolean isLastChild,
                             View convertView, ViewGroup parent) {
        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(mGroups.get(groupPosition).get(childPosition).getViewCode(), null);
        }

        mGroups.get(groupPosition).get(childPosition).setChild(convertView);
        return convertView;
    }

    public boolean isChildSelectable(int groupPosition, int childPosition){
        return true;
    }
}
