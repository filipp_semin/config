package com.example.config;

import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by Филипп on 25.08.2017.
 */

public class TextChild extends Child {

    private TextView textView;
    private Button button;
    private EditText editText;

    public TextChild(String name, Context context){
        super(name, context);
        viewCode = R.layout.child_view;
    }

    public void setChild(View convertView){
        textView = (TextView) convertView.findViewById(R.id.textChild);
        textView.setText(name);

        button   = (Button) convertView.findViewById(R.id.buttonChild);
        editText = (EditText) convertView.findViewById(R.id.editTextChild);
        button.setOnClickListener( new View.OnClickListener(){
            public void onClick(View view){
                Toast.makeText(mContext, textView.getText(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
