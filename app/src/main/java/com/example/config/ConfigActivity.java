package com.example.config;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ExpandableListView;

import java.util.ArrayList;

public class ConfigActivity extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_config);

        ExpandableListView listView = (ExpandableListView)findViewById(R.id.exListView);

        ArrayList<String> groupNames = new ArrayList<String>();
        groupNames.add("Volume");
        groupNames.add("Filters");
        groupNames.add("Debug");

        ArrayList<ArrayList<Child>> groups = new ArrayList<ArrayList<Child>>();

        ArrayList<Child> children1 = new ArrayList<Child>();
        ArrayList<Child> children2 = new ArrayList<Child>();
        ArrayList<Child> children3 = new ArrayList<Child>();

        children1.add(new TextChild("Pre-Amp",getApplicationContext()));
        children1.add(new TextChild("Amp",getApplicationContext()));
        children1.add(new TextChild("Post Amp",getApplicationContext()));
        groups.add(children1);
        for(int i = 0; i < 12; i++)
            children2.add(new TextChild("Filter" + i,getApplicationContext()));
        groups.add(children2);
        children3.add(new TextChild("White noise",getApplicationContext()));
        children3.add(new TextChild("Sin wave",getApplicationContext()));
        children3.add(new SwitchChild("Input",getApplicationContext()));
        groups.add(children3);

        ExpListAdapter adapter = new ExpListAdapter(getApplicationContext(), groups, groupNames);
        listView.setAdapter(adapter);
    }
}
